<p align="left">
<kbd>
<img style="border-radius:20px" height="150px" src="https://raw.githubusercontent.com/Realm-Labs/.github/refs/heads/main/profile/03FE2BB6-57BE-4259-B538-2A04307904E4.jpeg">
</kbd>
</p>
<h1 align="left">Realm Assets</h1>
<p align="left">Game files for 3kh0 v5, and 3kh0 X.</p>

All credits to [3kh0's game assets](https://gitlab.com/3kh0/3kh0-assets).
